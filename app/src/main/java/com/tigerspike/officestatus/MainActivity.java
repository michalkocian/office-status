package com.tigerspike.officestatus;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private TextView morning;
    private TextView morningLabel;
    private TextView afternoon;
    private TextView afternoonLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        morning = findViewById(R.id.tv_morning);
        morningLabel = findViewById(R.id.tv_morning_label);
        afternoon = findViewById(R.id.tv_afternoon);
        afternoonLabel = findViewById(R.id.tv_afternoon_label);

        setDate();
        initButtons();
    }

    private void setDate() {
        String dayValue = (String) DateFormat.format("dd", Calendar.getInstance()); // 20
        String monthString = (String) DateFormat.format("MMM", Calendar.getInstance()); // Jun

        TextView day = findViewById(R.id.day);
        TextView month = findViewById(R.id.month);

        day.setText(dayValue);
        month.setText(monthString.toUpperCase());
    }

    private void initButtons() {
        morning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (morning.getText().equals("IN")) {
                    morning.setText(R.string.out);
                    morning.setTextColor(Color.BLACK);
                    morningLabel.setTextColor(Color.BLACK);
                    morningLabel.setBackgroundResource(R.drawable.chevron_orange);
                    morning.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.out));
                } else {
                    morning.setBackgroundResource(R.drawable.in);
                    morning.setTextColor(Color.WHITE);
                    morningLabel.setTextColor(Color.WHITE);
                    morningLabel.setBackgroundResource(R.drawable.chevron_white);
                    morning.setText(R.string.in);
                }
            }
        });

        afternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (afternoon.getText().equals("IN")) {
                    afternoon.setText(R.string.out);
                    afternoon.setTextColor(Color.BLACK);
                    afternoonLabel.setTextColor(Color.BLACK);
                    afternoonLabel.setBackgroundResource(R.drawable.chevron_orange);
                    afternoon.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.out));
                } else {
                    afternoon.setBackgroundResource(R.drawable.in);
                    afternoon.setTextColor(Color.WHITE);
                    afternoonLabel.setTextColor(Color.WHITE);
                    afternoonLabel.setBackgroundResource(R.drawable.chevron_white);
                    afternoon.setText(R.string.in);
                }
            }
        });
    }
}
